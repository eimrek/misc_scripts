#!/usr/bin/env bash

pngs=`ls *.png`

for png in ${pngs}
do
    echo ${png}
    image_bbox_crop.py $png $png
done

