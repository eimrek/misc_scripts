#!/usr/bin/env python3
from ase import Atom, Atoms
from ase.io import read, write

import numpy as np
import sys
import os

if len(sys.argv) == 1:
    print('Please specify file name.')
    exit()


def write_atoms_with_numbers(atoms, filename):

    s = f"{len(atoms)}\n"
    s += "comment\n"
    for a in atoms:
        p = a.position
        symb = a.symbol
        if a.tag != 0:
            symb += str(a.tag)
        s += "{} {} {} {}\n".format(symb, p[0], p[1], p[2])

    with open(filename, 'w') as f:
        f.write(s)


for filename in sys.argv[1:]:
    try:
        a = read(filename)
        fname, ext = os.path.splitext(filename)
        new_fname = f"{fname}_ct{ext}"
        write_atoms_with_numbers(a, new_fname)
    except Exception as e:
        print(f"File {filename} conversion failed.")
        print(e)

