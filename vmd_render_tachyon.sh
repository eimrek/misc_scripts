#!/usr/bin/env bash

GEO_FILE=$1
FNAME=$(basename -- "$GEO_FILE")
EXT="${FNAME##*.}"
FNAME="${FNAME%.*}"

if [ "$EXT" != "xyz" ] && [ "$EXT" != "cube" ]; then
	echo "File is not XYZ or CUBE. Exiting."
	exit 1
fi

NAME=$FNAME
ISOVAL_STR=""
if [ -n "$2" ]; then
	read -r -d '' ISOVAL_STR <<- EOM
mol modstyle 3 0 Isosurface $2 0 0 0 1 1
mol modstyle 4 0 Isosurface -$2 0 0 0 1 1
mol showrep 0 3 1
mol showrep 0 4 1
	EOM
	NAME="$NAME-iv$2"
fi

BBOX=`atoms_bbox.py $GEO_FILE`

# VMD commands to rotate so that X is always longest direction
INIT_ORIENT_VMD=`vmd_orient_commands.py "$BBOX"`

BBOX_X=`python -c "print(max([float(x) for x in '$BBOX'.split()]))"`

echo "BBOX_X: $BBOX_X"

BBOX_X_HALF=`python -c "print($BBOX_X/2.0)"`

SCALE=`python -c "import numpy as np; print(np.sqrt(60/($BBOX_X+15)))"`

SCALE=1.8

if (( $(echo "$BBOX_X > 50" |bc -l) )); then
	SCALE=3.4
fi

echo "SCALE: $SCALE"

# Tachyon internal render command:
# render TachyonInternal $FNAME-z.bmp display %s

RES="5760 3072"

read -r -d '' RENDER_STR <<- EOM
    display resetview
    scale by $SCALE
    $INIT_ORIENT_VMD

    render Tachyon $FNAME-z.dat "/usr/local/lib/vmd/tachyon_LINUXAMD64" -res $RES -aasamples 12 %s -format TARGA -o %s.tga

    rotate x by -90.000000
    render Tachyon $FNAME-y.dat "/usr/local/lib/vmd/tachyon_LINUXAMD64" -res $RES -aasamples 12 %s -format TARGA -o %s.tga
    
    rotate y by -90.000000
    translate by 0.000000 0.000000 -$BBOX_X_HALF
    
    render Tachyon $FNAME-x.dat "/usr/local/lib/vmd/tachyon_LINUXAMD64" -res $RES -aasamples 12 %s -format TARGA -o %s.tga
    rotate y by 90
    rotate x by 90
    rotate x by -45
    rotate y by 45
    rotate z by 22.5
    render Tachyon $FNAME-iso.dat "/usr/local/lib/vmd/tachyon_LINUXAMD64" -res $RES -aasamples 12 %s -format TARGA -o %s.tga
EOM

echo -e "logfile console\n$ISOVAL_STR\n$RENDER_STR" | vmd $GEO_FILE -dispdev win -size 1920 1024 -eofexit

convert $FNAME-z.dat.tga z-$NAME.png 
convert $FNAME-x.dat.tga x-$NAME.png 
convert $FNAME-y.dat.tga y-$NAME.png 
convert $FNAME-iso.dat.tga iso-$NAME.png 

rm $FNAME-*.dat.tga

rm $FNAME-*.dat

image_bbox_crop.py z-$NAME.png z-$NAME.png
image_bbox_crop.py y-$NAME.png y-$NAME.png
image_bbox_crop.py x-$NAME.png x-$NAME.png
image_bbox_crop.py iso-$NAME.png iso-$NAME.png


