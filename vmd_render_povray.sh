#!/usr/bin/env bash

GEO_FILE=$1
FNAME=$(basename -- "$GEO_FILE")
EXT="${FNAME##*.}"
FNAME="${FNAME%.*}"

if [ "$EXT" != "xyz" ] && [ "$EXT" != "cube" ]; then
	echo "File is not XYZ or CUBE. Exiting."
	exit 1
fi

NAME=$FNAME
ISOVAL_STR=""
if [ -n "$2" ]; then
	read -r -d '' ISOVAL_STR <<- EOM
mol modstyle 3 0 Isosurface $2 0 0 0 1 1
mol modstyle 4 0 Isosurface -$2 0 0 0 1 1
mol showrep 0 3 1
mol showrep 0 4 1
	EOM
	NAME="$NAME-iv$2"
fi

BBOX=`atoms_bbox.py $GEO_FILE`

# VMD commands to rotate so that X is always longest direction
INIT_ORIENT_VMD=`vmd_orient_commands.py "$BBOX"`

BBOX_X=`python -c "print(max([float(x) for x in '$BBOX'.split()]))"`

BBOX_Y=`python -c "print(sorted([float(x) for x in '$BBOX'.split()])[1])"`

echo "BBOX_X: $BBOX_X"
echo "BBOX_Y: $BBOX_Y"

BBOX_X_HALF=`python -c "print($BBOX_X/2.0)"`

SCALE=`python -c "import numpy as np; print(np.sqrt(np.min([155/($BBOX_X+15), 80/($BBOX_Y+15)])))"`

#SCALE=1.8

#if (( $(echo "$BBOX_X > 50" |bc -l) )); then
#	SCALE=3.4
#fi

echo "SCALE: $SCALE"

# GENERATE POVRAY FILES

read -r -d '' RENDER_STR <<- EOM
    display resetview
    scale by $SCALE
    $INIT_ORIENT_VMD

    render POV3 z-$NAME.pov

    rotate x by -90.000000
    render POV3 y-$NAME.pov

    rotate y by -90.000000
    translate by 0.000000 0.000000 -$BBOX_X_HALF

    render POV3 x-$NAME.pov
    rotate y by 90
    rotate x by 90
    rotate x by -45
    rotate y by 45
    rotate z by 22.5
    render POV3 iso-$NAME.pov
EOM

echo -e "logfile console\n$ISOVAL_STR\n$RENDER_STR" | vmd $GEO_FILE -dispdev text -size 1920 1024 -eofexit

# Resolution
W=5760
H=3072

povray +W$W +H$H -Iz-$NAME.pov -D +A +FN +UA
povray +W$W +H$H -Iy-$NAME.pov -D +A +FN +UA
povray +W$W +H$H -Ix-$NAME.pov -D +A +FN +UA
povray +W$W +H$H -Iiso-$NAME.pov -D +A +FN +UA

rm *$NAME.pov

image_bbox_crop.py z-$NAME.png z-$NAME.png
image_bbox_crop.py y-$NAME.png y-$NAME.png
image_bbox_crop.py x-$NAME.png x-$NAME.png
image_bbox_crop.py iso-$NAME.png iso-$NAME.png


