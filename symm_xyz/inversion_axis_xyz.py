#!/usr/bin/env python
import numpy as np
import ase
import ase.io
import ase.visualize

import os
import argparse

parser = argparse.ArgumentParser(description='mirror xyz')
parser.add_argument('xyz_file', type=str, help='xyz file')
parser.add_argument('atom1', type=int, help='First index defining the inversion axis')
parser.add_argument('atom2', type=int, help='Second index defining the inversion axis')
parser.add_argument('--tol', type=float, default=0.1, help='tolerance to symmetrize overlapping atoms')

parsed_args = parser.parse_args()
filename = parsed_args.xyz_file
i1 = parsed_args.atom1
i2 = parsed_args.atom2
tol = parsed_args.tol

filename_noext = os.path.splitext(filename)[0]

geom = ase.io.read(filename)
# remove empty comment, which sometimes causes problems
geom.info.pop('', None) 

at1 = geom[i1]
at2 = geom[i2]

inv_axis = at2.position - at1.position
inv_axis /= np.linalg.norm(inv_axis)

inverted_geom = geom.copy()

for at in geom:
    at_a1 = at1.position - at.position
    to_axis = at_a1 - np.dot(at_a1, inv_axis) * inv_axis
    new_pos = at.position + 2*to_axis
    inverted_geom.append(ase.Atom(at.symbol, new_pos))

ase.visualize.view(inverted_geom)
