#!/usr/bin/env python
import numpy as np
import ase
import ase.io
import ase.visualize

import os
import argparse

parser = argparse.ArgumentParser(description='mirror xyz')
parser.add_argument('xyz_file', type=str, help='xyz file')
parser.add_argument('atom1', type=int, help='First index defining the inversion axis')
parser.add_argument('atom2', type=int, help='Second index defining the inversion axis')
parser.add_argument('axis', type=str, help='Axis (x, y or z) to orient to the atoms')

parsed_args = parser.parse_args()
filename = parsed_args.xyz_file
i1 = parsed_args.atom1
i2 = parsed_args.atom2
axis = parsed_args.axis

filename_noext = os.path.splitext(filename)[0]

geom = ase.io.read(filename)
# remove empty comment, which sometimes causes problems
geom.info.pop('', None)

at1 = geom[i1]
at2 = geom[i2]

vec = at2.position - at1.position

if axis != 'x' and axis != 'y' and axis != 'z':
    print("Error: wrong axis.")
    exit()

geom.rotate(vec, axis, center=at1.position)

geom.translate(-at1.position)

ase.visualize.view(geom)
