#!/usr/bin/env python

import subprocess
import os

import sys

import ase.io

import numpy as np

from sklearn.decomposition import PCA

from scipy.spatial.transform import Rotation

ang_2_bohr = 1.0/0.52917721067


if len(sys.argv) == 1:
    print("Please provide a file name.")
    sys.exit(1)

input_file = sys.argv[1]

basename = os.path.basename(input_file)
name, ext = os.path.splitext(basename)

if ext not in ['.xyz', '.cube']:
    print("File is not .xyz or .cube, exiting!")
    sys.exit(1)

if len(sys.argv) > 2:
    isov_inp = sys.argv[2]
    ext_name = f'{name}-{isov_inp}'
    isov_str = f"""
mol modstyle 3 0 Isosurface {isov_inp} 0 0 0 1 1
mol modstyle 4 0 Isosurface -{isov_inp} 0 0 0 1 1
mol showrep 0 3 1
mol showrep 0 4 1
"""
else:
    ext_name = name
    isov_str = ""


def read_cube_atoms(filename):

    f = open(filename, 'r')
    title = f.readline().rstrip()
    comment = f.readline().rstrip()

    line = f.readline().split()
    natoms = np.abs(int(line[0]))

    origin = np.array(line[1:], dtype=float)

    cell_n = np.empty(3,dtype=int)
    cell = np.empty((3, 3))
    for i in range(3):
        n, x, y, z = [float(s) for s in f.readline().split()]
        cell_n[i] = int(n)
        cell[i] = n * np.array([x, y, z])

    numbers = np.empty(natoms, int)
    positions = np.empty((natoms, 3))
    for i in range(natoms):
        line = f.readline().split()
        numbers[i] = int(line[0])
        positions[i] = [float(s) for s in line[2:]]

    positions /= ang_2_bohr # convert from bohr to ang
    return ase.Atoms(numbers=numbers, positions=positions, cell=cell)

def iter_rotation(pos, vary='x', goal='z', mode='min'):
    
    goal_i = {'x': 0, 'y': 1, 'z': 2}[goal]
    
    f = 1.0 if mode == 'min' else -1.0

    ext0 = np.ptp(pos[:, goal_i])

    new_pos = np.copy(pos)

    angle_step = 0.2

    # find initial rot direction
    r = Rotation.from_euler(vary, angle_step, degrees=True)
    iter_pos = r.apply(new_pos)
    if f * (np.ptp(iter_pos[:, goal_i]) - ext0) > 0:
        angle_step *= -1

    cumulative_ang = 0.0

    while True:

        r = Rotation.from_euler(vary, angle_step, degrees=True)
        iter_pos = r.apply(new_pos)
        if f * (np.ptp(iter_pos[:, goal_i]) - ext0) < 0:
            new_pos = iter_pos
            cumulative_ang += angle_step
            ext0 = np.ptp(iter_pos[:, goal_i])
        else:
            r = Rotation.from_euler(vary, -angle_step, degrees=True)
            iter_pos = r.apply(new_pos)
            ext0 = np.ptp(iter_pos[:, goal_i])
            angle_step /= 2
        if np.abs(angle_step) < 0.001:
            break

    return new_pos, cumulative_ang

def orient_and_bbox(ase_atoms, iter_corr=False):
    # STEP 1: PCA
    pca = PCA(n_components=3)
    pos = ase_atoms.positions
    new_pos = pca.fit_transform(pos)

    pc = pca.components_
    for r in pc:
        print("{:5.2f} {:5.2f} {:5.2f}".format(*r))
    print(np.linalg.det(pc))

    refl = False
    if np.linalg.det(pc) < -0.5:
        refl = True
        pc[:, 0] *= -1

    # find rotations
    sy = np.sqrt(pc[0,0]**2 + pc[1,0]**2)
    singular = sy < 1e-6
    if not singular:
        theta_x = np.arctan2(pc[2,1], pc[2,2])
        theta_y = np.arctan2(-pc[2,0], sy)
        theta_z = np.arctan2(pc[1,0], pc[0,0])
    else:
        theta_x = np.arctan2(-pc[1,2], pc[1,1])
        theta_y = np.arctan2(-pc[2,0], sy)
        theta_z = 0

    rot = np.array([theta_x, theta_y, theta_z]) * 180 / np.pi

    if iter_corr:
        iter_pos, ang_z = iter_rotation(new_pos, vary='z', goal='y', mode='min')
        iter_pos, ang_x = iter_rotation(iter_pos, vary='x', goal='z', mode='min')
        iter_pos, ang_y = iter_rotation(iter_pos, vary='y', goal='z', mode='min')

        rot[0] += ang_x
        rot[1] += ang_y
        rot[2] += ang_z
        new_pos = iter_pos

    bbox = np.ptp(new_pos, axis=0)

    print(rot)

    return bbox, rot, refl, new_pos


# find orientation and BBOX

if ext == '.xyz':
    geo = ase.io.read(input_file)
elif ext == '.cube':
    geo = read_cube_atoms(input_file)
    
bbox, rot, _, _ = orient_and_bbox(geo, iter_corr=True)

print(bbox, rot)

print(bbox)
scale = np.clip(0.35 * bbox[0]/bbox[1] + 1.1, 1.1, 2.5)
scale *= np.clip((1.0 - 800/(bbox[0]+bbox[1])**3), 0.20, 1.0)

print(f"SCALE {scale}")

render_str = f"""
display resetview
scale by {scale}
rotate x by {rot[0]}
rotate y by {rot[1]}
rotate z by {rot[2]}

render POV3 z-{ext_name}.pov

rotate x by -90.000000
render POV3 y-{ext_name}.pov

rotate y by -90.000000
translate by 0.000000 0.000000 -{bbox[0]/2}

render POV3 x-{ext_name}.pov
rotate y by 90
rotate x by 90
rotate x by -45
rotate y by 45
rotate z by 22.5
render POV3 iso-{ext_name}.pov
"""

stdin_data = f"logfile console\n{isov_str}\n{render_str}"


from subprocess import Popen, PIPE, STDOUT
p = Popen(['vmd', input_file, '-dispdev', 'text', '-size', '1920', '1024', '-eofexit'],
          stdout=PIPE, stdin=PIPE, stderr=PIPE)
stdout_data = p.communicate(input=stdin_data.encode())[0]

# Resolution
w, h = 2*1920, 2*1024

for orient in ['x', 'y', 'z', 'iso']:
    subprocess.run(['povray', f'+W{w}', f'+H{h}', f'-I{orient}-{ext_name}.pov', '-D', '+A', '+FN', '+UA'])
    
subprocess.run([f'rm *{ext_name}.pov'], shell=True)

for orient in ['z', 'y', 'x', 'iso']:
    subprocess.run([f'image_bbox_crop.py {orient}-{ext_name}.png {orient}-{ext_name}.png'], shell=True)

