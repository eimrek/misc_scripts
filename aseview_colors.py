#!/usr/bin/env python3
from ase.io import read, write
from ase.visualize import view

import sys

import io

if len(sys.argv) == 1:
    print('Please specify file name.')
    exit()

filename = sys.argv[1]

raw_data = ""

for i_line, line in enumerate(open(filename, 'r').readlines()):
    sline = line.split()
    if " =T " in line:
        raw_data += line.replace(" =T ", " ")
    elif i_line == 0 or i_line == 1:
        raw_data += line
    else:
        element = sline[0]
        if element[-1] == '1':
            element = 'O'
        elif element[-1] == '2':
            element = 'N'
        elif element == 'Bq':
            element = 'X'
        elif element[-1] == 'G':
            element = element[:-1]
        raw_data += (element + " " + " ".join(sline[1:]) + "\n")

iofile = io.StringIO()
iofile.write(raw_data)

a = read(iofile, format="extxyz")

# Also parse tags
for at in a:
    if at.tag == 1:
        at.symbol = 'O'
    elif at.tag == 2:
        at.symbol = 'N'

view(a)

