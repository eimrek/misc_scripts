#!/usr/bin/env python
import numpy as np
import time

import argparse

import ase.io.cube

parser = argparse.ArgumentParser(
    description='Averages the cube file to 1d')
parser.add_argument(
    'cubefile',
    metavar='FILENAME',
    help='.cube file')

args = parser.parse_args()

data, atoms = ase.io.cube.read_cube_data(args.cubefile)

print("Max of cube:", np.max(data))

# average to 1d and output max
avg_x = np.mean(data, axis=(1, 2))
print("Cube averaged to x axis. Max:", np.max(avg_x))

avg_y = np.mean(data, axis=(0, 2))
print("Cube averaged to y axis. Max:", np.max(avg_y))

avg_z = np.mean(data, axis=(0, 1))
print("Cube averaged to z axis. Max:", np.max(avg_z))

print("Max of the three:")
print(np.max([np.max(avg_x), np.max(avg_y), np.max(avg_z)]))
