#!/usr/bin/env bash

cubes=`ls *.cube`

for cube in ${cubes}
do
    echo ${cube}
    vmd_render_povray.py $cube $1
done

if [ -n "$2" ]; then
	gauss_orb_labels.py $2
fi
