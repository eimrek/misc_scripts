#!/usr/bin/env python3
import numpy as np
import sys
import os

import ase.io

ang_2_bohr = 1.0/0.52917721067

if len(sys.argv) == 1:
    print('Please specify the bounding box.')
    exit()

bbox_str = sys.argv[1]

bbox = [float(x) for x in bbox_str.split()]

result_str = ""

# rotate largest dim to x
if bbox[1] >= bbox[0] and bbox[1] >= bbox[2]:
    result_str += "rotate z by 90.0; "
    a = bbox[0]
    bbox[0] = bbox[1]
    bbox[1] = a
elif bbox[2] >= bbox[0] and bbox[2] >= bbox[1]:
    result_str += "rotate y by 90.0; "
    a = bbox[0]
    bbox[0] = bbox[2]
    bbox[2] = a

# rotate the next largest to y
if bbox[2] > bbox[1]:
    result_str += "rotate x by 90.0; "

print(result_str)
