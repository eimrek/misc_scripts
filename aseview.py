#!/usr/bin/env python3
from ase.io import read, write
from ase.visualize import view

import tempfile

import sys

import io

if len(sys.argv) == 1:
    print('Please specify file name.')
    exit()

filename = sys.argv[1]

raw_data = ""

tags = []

for i_line, line in enumerate(open(filename, 'r').readlines()):
    sline = line.split()
    if " =T " in line:
        raw_data += line.replace(" =T ", " ")
    elif i_line == 0 or i_line == 1:
        raw_data += line
    else:
        element = sline[0]
        if element[-1].isnumeric():
            tags.append(int(element[-1]))
            element = element[:-1]
        else:
            tags.append(0)
        if element == 'Bq':
            element = 'X'
        if element[-1] == 'G':
            element = element[:-1]
        raw_data += (element + " " + " ".join(sline[1:]) + "\n")

#iofile = io.StringIO()
#iofile.write(raw_data)

with tempfile.NamedTemporaryFile(mode='w') as tf:
    tf.write(raw_data)
    tf.seek(0)
    a = read(tf.name, format="extxyz")

if any(t != 0 for t in tags):
    a.set_tags(tags)

view(a)

