#!/usr/bin/env python
import numpy as np
import scipy
import scipy.io
import scipy.ndimage
import time
import copy

import skimage.measure

import argparse

import ase.io.cube

import matplotlib.pyplot as plt

ang_2_bohr = 1.0/0.52917721067
hart_2_ev = 27.21138602
ry_2_ev = 13.605698065894

parser = argparse.ArgumentParser(
    description='Projects a cube file to specified plane(s)')
parser.add_argument(
    'cubefile',
    metavar='FILENAME',
    help='.cube file')
parser.add_argument(
    '--out_dir',
    required=True,
    metavar='FOLDER',
    help='Output directory.')
parser.add_argument(
    '--sym_radius',
    type=float,
    default=0.0,
    metavar='R',
    help=("Symmetrization radius [ang] specifies sphere around atoms where"
          "the potential is symmetrized. If R<=0, no symmetrization is done."))

parser.add_argument(
    '--eq_peaks',
    action='store_true',
    help="Average and equalize the potential around same element atoms in the sym. region.")

parser.add_argument(
    '--align_peaks',
    action='store_true',
    help="Instead of finding the voxel with peak value, find nearest voxel to atom.")


time0 = time.time()
args = parser.parse_args()

### -----------------------------------
### Load and setup Cube data
### -----------------------------------

data, atoms = ase.io.cube.read_cube_data(args.cubefile)

shape = data.shape
cell = atoms.cell
dv = cell/shape

###--------------------------------
### Symmetrize the potential around atoms
### --------------------------------

if args.sym_radius <= 0.0:
    print("No symmetrization!")
else:

    time1 = time.time()

    cn = data.shape

    # Make a grid with (0.0, 0.0, 0.0) at indexes [0, 0, 0] and ...

    x_arr_ref = np.concatenate([np.arange(-dv_rot[0], -plane_x/2-dv_rot[0], -dv_rot[0])[::-1], np.arange(0.0, plane_x/2, dv_rot[0])])
    y_arr_ref = np.concatenate([np.arange(-dv_rot[1], -plane_y/2-dv_rot[1], -dv_rot[1])[::-1], np.arange(0.0, plane_y/2, dv_rot[1])])
    z_arr_ref = np.concatenate([np.arange(-dv_rot[2], -cell_proj_width/2-dv_rot[2], -dv_rot[2])[::-1], np.arange(0.0, cell_proj_width/2, dv_rot[2])])

    x_izero = np.where(x_arr_ref == 0.0)[0][0]
    y_izero = np.where(y_arr_ref == 0.0)[0][0]
    z_izero = np.where(z_arr_ref == 0.0)[0][0]

    symm_data = np.copy(interp_data)
    
    # Find peaks of the potential near atomic positions
    peaks = {} # peaks[atom_nr] = [[indexes]] 

    processed_pos = []

    for atom in atoms:
        pos_unrot = atom.position # position in the original basis
        num_images = 6 # this will hopefully cover the whole projection area...
        for imag_x in range(-num_images, num_images+1):
            for imag_y in range(-num_images, num_images+1):
                for imag_z in range(-num_images, num_images+1):
                    pos_image = np.copy(pos_unrot)
                    pos_image[0] += imag_x*cell[0]
                    pos_image[1] += imag_y*cell[1]
                    pos_image[2] += imag_z*cell[2]

                    # position in the rotated basis
                    pos = np.dot(trans_mat_ir, pos_image)
                    
                    # if image is outside the projected cell, discard...
                    eps = 1e-2
                    if pos[0] < -eps or pos[0] > plane_x+eps:
                        continue
                    if pos[1] < -eps or pos[1] > plane_y+eps:
                        continue 
                    if pos[2] < -eps or pos[2] > cell_proj_width+eps:
                        continue
                    
                    # It might be that "atoms" contains equivalent atoms, so skip if we already processed it
                    for proc_pos in processed_pos:
                        if np.linalg.norm(proc_pos-pos) < 1e-3:
                            print("Skipping equivalent atom!")
                            continue
                    processed_pos.append(pos)

                    # atomic position on the grid
                    int_shift = np.round(pos/dv_rot).astype(int)

                    if args.align_peaks:
                        peak_inds = np.copy(int_shift)
                    else:
                        # find the potential peak around the atomic position
                        neighbors = 4
                        # select the neighborhood of the atom (with periodic boundaries)
                        # NB: actually we shouldn't use PBC !!! (therefore maximum instead of 'wrap')
                        
                        min_idx = np.pad(symm_data, neighbors, mode='maximum')[
                                        int_shift[0]:int_shift[0]+2*neighbors+1,
                                        int_shift[1]:int_shift[1]+2*neighbors+1,
                                        int_shift[2]:int_shift[2]+2*neighbors+1].flatten().argmin()
                        
                        neigh_shape = 2*neighbors+1
                        ind_x = int(min_idx/(neigh_shape*neigh_shape)) + int_shift[0]-neighbors
                        ind_y = int((min_idx % (neigh_shape*neigh_shape))/neigh_shape) + int_shift[1]-neighbors
                        ind_z = int(min_idx % neigh_shape) + int_shift[2]-neighbors
                        
                        peak_inds = np.array([ind_x, ind_y, ind_z])

                    if atom.number in peaks:
                        peaks[atom.number].append(peak_inds)
                    else:
                        peaks[atom.number] = [peak_inds]

    if args.eq_peaks:
        # Go through the peaks and save the potential dependence on radius
        pot_dep = {}

        for at_nr in peaks:
            if at_nr not in pot_dep:
                pot_dep[at_nr] = []
            for peak_inds in peaks[at_nr]:
                
                # Roll the grid so that the 0 gridpoint is nearest to the peak
                # NB: actually shift instead, because we don't have PBCs
                #x_arr_shift = np.roll(x_arr_ref, peak_inds[0]-x_izero)
                #y_arr_shift = np.roll(y_arr_ref, peak_inds[1]-y_izero)
                #z_arr_shift = np.roll(z_arr_ref, peak_inds[2]-z_izero)
                x_arr_shift = scipy.ndimage.interpolation.shift(x_arr_ref, peak_inds[0]-x_izero, cval=1e3)
                y_arr_shift = scipy.ndimage.interpolation.shift(y_arr_ref, peak_inds[1]-y_izero, cval=1e3)
                z_arr_shift = scipy.ndimage.interpolation.shift(z_arr_ref, peak_inds[2]-z_izero, cval=1e3)

                # Build up R grid centered around the peak
                x_grid_sh, y_grid_sh, z_grid_sh = np.meshgrid(x_arr_shift, y_arr_shift, z_arr_shift, indexing='ij')
                r_grid = np.sqrt(x_grid_sh**2 + y_grid_sh**2 + z_grid_sh**2)
                
                dr = np.mean(dv_rot)/3
                
                pot_dep[at_nr].append([])

                num_intervals = int(args.sym_radius/dr)
                
                for ir in range(num_intervals):
                    shell_inds = (r_grid >= ir*dr) & (r_grid < (ir+1)*dr)
                    if not shell_inds.any():
                        continue
                    shell_avg = np.mean(symm_data[shell_inds])
                    pot_dep[at_nr][-1].append(shell_avg)

        # average the potential dependence on radius for all atoms of the same elements
        avg_pot_dep = {}
        print_bool = True
        for at_nr in pot_dep:
            avg_pd = np.zeros(len(pot_dep[at_nr][0]))
            for pd in pot_dep[at_nr]:
                avg_pd += np.array(pd)
            avg_pd /= len(pot_dep[at_nr])
            avg_pot_dep[at_nr] = avg_pd
            if print_bool:
                print("Number of pixels equalized %d" % len(avg_pd))
                print_bool = False
        

    for at_nr in peaks:
        for i_peak, peak_inds in enumerate(peaks[at_nr]):

            # Roll the grid so that the 0 gridpoint is nearest to the peak
            # NB: actually shift instead, because we don't have PBCs
            #x_arr_shift = np.roll(x_arr_ref, peak_inds[0])
            #y_arr_shift = np.roll(y_arr_ref, peak_inds[1])
            #z_arr_shift = np.roll(z_arr_ref, peak_inds[2])
            x_arr_shift = scipy.ndimage.interpolation.shift(x_arr_ref, peak_inds[0]-x_izero, cval=1e3)
            y_arr_shift = scipy.ndimage.interpolation.shift(y_arr_ref, peak_inds[1]-y_izero, cval=1e3)
            z_arr_shift = scipy.ndimage.interpolation.shift(z_arr_ref, peak_inds[2]-z_izero, cval=1e3)

            
            # Build up R grid centered around the peak
            x_grid_sh, y_grid_sh, z_grid_sh = np.meshgrid(x_arr_shift, y_arr_shift, z_arr_shift, indexing='ij')
            r_grid = np.sqrt(x_grid_sh**2 + y_grid_sh**2 + z_grid_sh**2)
            
            dr = np.mean(dv_rot)/3

            num_intervals = int(args.sym_radius/dr)
            
            # Smooth transition from "symmetrized region" to normal region
            transition_radius = 0.2*args.sym_radius
            num_intervals = int(args.sym_radius/dr)
            
            count = 0

            for ir in range(num_intervals):
                shell_inds = (r_grid >= ir*dr) & (r_grid < (ir+1)*dr)
                if not shell_inds.any():
                    continue
                
                if args.eq_peaks:
                    new_val = avg_pot_dep[at_nr][count]
                    count += 1
                else:
                    new_val = np.mean(symm_data[shell_inds])
                
                if ir*dr < args.sym_radius - transition_radius:
                    symm_data[shell_inds] = new_val
                else:
                    # Make a smooth transition
                    trans_factor = (ir*dr-(args.sym_radius - transition_radius))/transition_radius # 0 at start of transition, 1 at end
                    symm_data[shell_inds] = trans_factor*symm_data[shell_inds] + (1-trans_factor)*new_val
        
    # Replace the data used for following with the symmetrized data
    interp_data = symm_data

    print("Symmetrization time %.4f s" % (time.time()-time1))
    

