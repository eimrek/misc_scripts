#!/usr/bin/env python3
import numpy as np
from ase import Atom
from ase.io import read,write
from ase.visualize import view
from operator import itemgetter
import ase
import ase.build

import os

import argparse


parser = argparse.ArgumentParser(description='Create gold substrate under molecule.')
parser.add_argument('structure_file', type=str, help='File containing the molecule.')

parser.add_argument(
    '--functional',
    type=str,
    required=False,
    default='PBE+D3',
    help = "XC functional and dispersion, supported: [PBE+D3, revPBE+D3BJ]")

args = parser.parse_args()
filename = args.structure_file

filename_noext = os.path.splitext(filename)[0]

# ---------------------------------------------------------------------------------
# INPUTS:

# OPTIMIZED PBE-D3 (bulk values for 5x5x5)
if args.functional == 'PBE+D3':
    lat_param = 4.1691
    dz_au_h    = 0.86519004 # Distance between Au and H layer
    dz_au_1_2  = 2.45156905 # Distance between Au top layer and 2nd layer
    dz_au_2_3  = 2.39444938 # Distance between Au 2nd layer and 3rd layer (bulk)

# OPTIMIZED revPBE-D3(BJ) (bulk values for 5x5x5)
elif args.functional == 'revPBE+D3BJ':
    lat_param = 4.1449971593488515
    dz_au_h    = 0.8796123915 # Distance between Au and H layer
    dz_au_1_2  = 2.4453144136 # Distance between Au top layer and 2nd layer
    dz_au_2_3  = 2.38418689 # Distance between Au 2nd layer and 3rd layer (bulk)

else:
    print("Error: functional not supported")
    sys.exit(1)

# ---------------------------------------------------------------------------------


mol = read(filename)

# remove empty comment, which sometimes causes problems
mol.info.pop('', None) 

nz = 4

atomsmol = len(mol)

x, y, z = zip(*mol.positions)

# Size of the cell containing exactly the molecule
cx=np.amax(x) - np.amin(x)
cy=np.amax(y) - np.amin(y)

print('Bounding box (x, y) for molecule is (%f, %f)'%(cx, cy))
inp = input('Add in x and y directions (e.g. "20 20"): ')
add_x, add_y = inp.split()
add_x = float(add_x); add_y = float(add_y)


# Size of the extended cell (preliminary)
cx = cx + add_x
cy = cy + add_y



# dimensions of a rectangular unit cell of Au with z pointing in (111) direction
Au_x = lat_param/np.sqrt(2)
Au_y = lat_param*np.sqrt(3/2)

# How many gold atomic layers can be fitted?
nx=int(np.rint(cx/Au_x))
ny=int(np.rint(cy/Au_y))

# And the x,y coordinates of the Au atoms in exact symmetric positions of the three layers of the unit cell
c1 = lat_param / np.sqrt(24)
c2 = lat_param / np.sqrt(8)
au_exact_xz = np.array([
    [[0.0, 2*c1], [c2, 5*c1]],
    [[0.0,  0.0], [c2, 3*c1]],
    [[0.0, 4*c1], [c2, 1*c1]]])

dz_au_bulk = lat_param / np.sqrt(3) # Distance between bulk Au layers

# Build up the unit cell of the gold slab with nz Au layers
au_slab = []
layer_z = 10.0
# H layer in au lattice x-y positions
au_slab.append([1, au_exact_xz[0, 0, 0], au_exact_xz[0, 0, 1], layer_z])
au_slab.append([1, au_exact_xz[0, 1, 0], au_exact_xz[0, 1, 1], layer_z])
# nz bulk layers
for i in range(nz):
    if i == 0:
        layer_z += dz_au_h
    elif i == nz - 2:
        layer_z += dz_au_2_3
    elif i == nz - 1:
        layer_z += dz_au_1_2
    else:
        layer_z += dz_au_bulk
    xy_pos = au_exact_xz[(i+1)%3]
    au_slab.append([79, xy_pos[0, 0], xy_pos[0, 1], layer_z])
    au_slab.append([79, xy_pos[1, 0], xy_pos[1, 1], layer_z])

au_slab = np.array(au_slab)

# determine cell size
vac_size = 40.0
slab_z_max = np.max(au_slab[:, 3])
slab_z_extent = slab_z_max - np.min(au_slab[:, 3])

cz = slab_z_extent + vac_size
cx = nx * Au_x
cy = ny * Au_y
print("Cell ABC: %f %f %f"%(cx, cy, cz))

# position molecule
#mol_slab_dist = 2.3
mol_slab_dist = float(input('Mol hover distance (def: 2.3; for planar 2.6): '))

mol.cell = (cx,cy,cz)
mol.pbc = (True,True,True)

# position molecule a bit above gold slab
mol.center()
mol_z_min = np.min(mol.positions[:,2])
mol.positions[:,2] += slab_z_max - mol_z_min + mol_slab_dist


# generate gold slab based on the rectangular unit cell
au_slab_raw = []
for i in range(nx):
    for j in range(ny):
        shift = np.array([0, i*Au_x, j*Au_y, 0])
        au_slab_raw.append(au_slab + shift)

au_slab_raw = np.concatenate(au_slab_raw)
au_slab = ase.Atoms(numbers=au_slab_raw[:,0], positions=au_slab_raw[:,1:])
au_slab = ase.build.sort(au_slab, tags=au_slab.get_positions()[:,2]*-1)

mol_on_au = mol + au_slab


mol.write(filename_noext+"_mol.xyz")

mol_on_au.write(filename_noext+"_au.xyz")

print("Molecule indexes from 1 to ",atomsmol)
print("Substrate indexes from ",atomsmol+1," to ", atomsmol+nx*ny*2*5)

print("Top 2 Au layers' indexes from ", atomsmol+1, " to ", atomsmol+nx*ny*2*2)
print("Bottom 3 layers' (2 Au and 1 H) indexes from ", atomsmol+nx*ny*2*2+1, " to ", atomsmol+nx*ny*2*5)
