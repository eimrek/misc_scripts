#!/bin/bash

# inspiration from
# https://gist.github.com/ctemplin/3c626af969c67e51d805

id=$(xprop -root 32x '\t$0' _NET_ACTIVE_WINDOW | cut -f 2)
class_string=$(xprop -id $id | awk '/WM_CLASS\(STRING\)/' | cut -d'=' -f2 | cut -d',' -f1)

if [ $class_string == '"xfce4-terminal"' ]; then
    # Get the path from titlebar, e.g. "Terminal - user@ub:~/folder1/folder2"
    name=$(xprop -id $id | awk '/_NET_WM_NAME/' | cut -d'"' -f2 | cut -d" " -f4 );
    name=$(echo "$name" | sed "s|~|$HOME|" )
    cwd=$name
elif [ $class_string == '"Thunar"' ]; then
    # In Thunar, by default, the titlebar does not include the path
    # Enable it by
    # xfconf-query --channel thunar --property /misc-full-path-in-title --create --type bool --set true
    name=$(xprop -id $id | awk '/_NET_WM_NAME/' | cut -d'"' -f2 | cut -d" " -f1 );
    name=$(echo "$name" | sed "s|~|$HOME|" )
    cwd=$name
else
    # For the rest, try to get the path via pwdx
    pid=$(xprop -id $id | awk '/_NET_WM_PID\(CARDINAL\)/' | cut -d'=' -f2)
    cwd=$(pwdx $pid | cut -d':' -f2 | cut -d' ' -f2)
fi

cwd=${cwd:-$(echo $HOME)}

# check if cwd is a directory
if [[ ! -d "$cwd" ]]; then
    cwd=$HOME
fi

if [ $1 == 'terminal' ]; then
    xfce4-terminal --default-working-directory $cwd
elif [ $1 == 'thunar' ]; then
    thunar $cwd
else
    echo $cwd
fi

