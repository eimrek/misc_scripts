#!/usr/bin/env python3
import numpy as np
import sys
import os

import ase.io

ang_2_bohr = 1.0/0.52917721067

if len(sys.argv) == 1:
    print('Please specify file name.')
    exit()

filename = sys.argv[1]

name, ext = os.path.splitext(filename)

def read_cube_atoms(filename):

    f = open(filename, 'r')
    title = f.readline().rstrip()
    comment = f.readline().rstrip()

    line = f.readline().split()
    natoms = np.abs(int(line[0]))

    origin = np.array(line[1:], dtype=float)

    cell_n = np.empty(3,dtype=int)
    cell = np.empty((3, 3))
    for i in range(3):
        n, x, y, z = [float(s) for s in f.readline().split()]
        cell_n[i] = int(n)
        cell[i] = n * np.array([x, y, z])

    numbers = np.empty(natoms, int)
    positions = np.empty((natoms, 3))
    for i in range(natoms):
        line = f.readline().split()
        numbers[i] = int(line[0])
        positions[i] = [float(s) for s in line[2:]]

    positions /= ang_2_bohr # convert from bohr to ang
    return ase.Atoms(numbers=numbers, positions=positions, cell=cell)


if ext == '.xyz':
    at = ase.io.read(filename)
elif ext == '.cube':
    at = read_cube_atoms(filename)
else:
    print("Error: file type not supported.")
    exit(1)

bbox = [
    np.max(at.positions[:, 0]) - np.min(at.positions[:, 0]),
    np.max(at.positions[:, 1]) - np.min(at.positions[:, 1]),
    np.max(at.positions[:, 2]) - np.min(at.positions[:, 2]),
]

print("%.4f %.4f %.4f" % tuple(bbox))
