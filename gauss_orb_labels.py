#!/usr/bin/env python3

import numpy as np

import os
import sys

import cclib

import subprocess

if len(sys.argv) != 2:
    print("Error: specify gaussian log.")
    sys.exit(1)

data = cclib.io.ccread(sys.argv[1])
property_dict = data.getattributes()

for file in os.listdir():
    if not file.endswith(".png"):
        continue
    
    if not file.startswith("iso-") and not file.startswith("z-"):
        continue

    if not 'homo' in file or "_label" in file:
        continue 
    
    orb_num = int(file.split('-')[1])
    spin = 1 if '-b_' in file else 0
    uks = True if ('-b_' in file or '-a_' in file) else False
    energy = property_dict['moenergies'][spin][orb_num-1]
    
    # Find orbital label
    tmp = file.split('homo')[1]
    wrt_homo = tmp[0]
    wrt_homo += tmp[1:].split('-')[0]
    wrt_homo = int(wrt_homo)
    
    label = ""
    if uks:
        label = u'α-' if spin == 0 else u'β-'
    if wrt_homo < 0:
        label += "HOMO%d" % wrt_homo
    elif wrt_homo == 0:
        label += "HOMO"
    elif wrt_homo == 1:
        label += "LUMO"
    else:
        label += "LUMO+%d" % (wrt_homo-1)
    
    if file.startswith("iso-"):
        pos = (0.15, 0.1)
        full_label = label +"\nE=%.3f eV" % energy
    elif file.startswith("z-"):
        pos = (0.5, 1.1)
        full_label = label + " E=%.3f eV" % energy
        
    print(file)    
    #print(full_label)
    subprocess.call(['image_add_label.py', file, full_label, str(pos[0]), str(pos[1])])
    

