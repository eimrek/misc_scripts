#!/usr/bin/env python3
from ase import Atom, Atoms
from ase.io import read, write

import numpy as np
import sys
import os

if len(sys.argv) == 1:
    print('Please specify file name.')
    exit()

for filename in sys.argv[1:]:

    name, ext = os.path.splitext(filename)
    
    if ext != '.xsd':
        print(f"{filename} is not an xsd file!")
        continue
    
    a = Atoms()
    
    with open(filename) as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            if line.startswith("<Atom3d"):
    
                if "XYZ=" not in line:
                    xyz=[0.0, 0.0, 0.0]
                else:
                    xyz_split = line.split("XYZ=\"")
                    xyz_split = xyz_split[1].split("\"")
                    xyz_str = xyz_split[0]
                    xyz = [float(x) for x in xyz_str.split(",")]
                
                if "Components" not in line:
                    continue
                components_split = line.split("Components=\"")
                components_split = components_split[1].split("\"")
                components = components_split[0]
                elem = components
                
                a.append(Atom(elem, xyz))
    
    x_extent = np.max(a.positions[:, 0]) - np.min(a.positions[:, 0])
    y_extent = np.max(a.positions[:, 1]) - np.min(a.positions[:, 1])
    z_extent = np.max(a.positions[:, 2]) - np.min(a.positions[:, 2])
    
    cell = (np.round(x_extent+8.0), np.round(y_extent+8.0), np.round(z_extent+8.0))
    
    a.cell = cell
    
    min_dist = np.sort(a.get_all_distances().flatten())[len(a)]
    
    if min_dist < 0.1:
        print("Error: min distance is %.2e, possibly a duplicate atom." % min_dist)
        sys.exit(1)
    
    print("Min distance: %.2f" % min_dist)
    print("Bounding box: (%.2f %.2f %.2f)" % (x_extent, y_extent, z_extent))
    print("Cell (BB+8+round): (%.2f %.2f %.2f)" % cell)
    print("In case of a periodic metal slab system:")
    print("  * Don't forget to change the cell manually!")
    print("  * Don't forget to reorder if needed (last atom: %s)" % (str(a[-1])))
    
    # Don't center! will ruin e.g. gold slab position wrt cell
    #a.center()
    
    write(name+'.xyz', a)
