#!/usr/bin/env python3
import numpy as np
import sys
import os

if len(sys.argv) == 1:
    print('Please specify file name.')
    exit()

filename = sys.argv[1]

name, ext = os.path.splitext(filename)

out_name = name + "_cropped" + ext
if len(sys.argv) > 2:
    out_name = sys.argv[2]

from PIL import Image, ImageDraw, ImageFont, ImageOps

image = Image.open(filename)

try:
    image = image.convert('RGBa')
    image = image.crop(image.getbbox())
except:
    inv_image = ImageOps.invert(image)
    image = image.crop(inv_image.getbbox())

image = image.convert('RGBA')
image.save(out_name)

