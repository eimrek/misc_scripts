#!/usr/bin/env bash

images=`ls *.bmp`

for image in ${images}
do
    filename=${image%.*}
    echo ${filename}
    convert ${filename}.bmp ${filename}.png
    rm ${filename}.bmp
done
