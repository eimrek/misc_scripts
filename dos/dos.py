import numpy as np
import matplotlib.pyplot as plt


discrete_values = [-0.5, -0.4, 0.1, 0.3, 0.5]
fwhm = 0.1

def gaussian(x, fwhm):
    sigma = fwhm/2.3548
    return np.exp(-x**2/(2*sigma**2))/(sigma*np.sqrt(2*np.pi))


dx = 0.01
xmin = np.min(discrete_values)-2*fwhm
xmax = np.max(discrete_values)+2*fwhm
x_arr = np.arange(xmin, xmax, dx)
y_arr = np.zeros(len(x_arr))

for disc_v in discrete_values:
    y_arr += gaussian(x_arr - disc_v, fwhm)


# -----------------
# Make the plot
plt.plot(x_arr, y_arr)

# add vertical lines for discrete values
for disc_v in discrete_values:
    plt.axvline(disc_v, linestyle='--', color='gray')

plt.xlim([xmin, xmax])
plt.show()
# -----------------
