#!/usr/bin/env bash

xyzs=`ls *.xyz`

for xyz in ${xyzs}
do
    echo ${xyz}
    vmd_render_povray.py $xyz
done
