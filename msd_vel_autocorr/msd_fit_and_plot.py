#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser(
    description='Fits a straight line through the MSD data and plots.')
parser.add_argument(
    'file',
    metavar='FILENAME',
    help='Output of mean_square_disp calculation')
parser.add_argument(
    '--fit_start',
    type=float,
    metavar='T',
    help='Value of time in [fs] where to start the fitting.')

args = parser.parse_args()

data = np.loadtxt(args.file)

t_arr = data[:, 1]
msd = data[:, 2]

fit_ind = t_arr > args.fit_start

# Fit a straight line
slope, intercept = np.polyfit(t_arr[fit_ind], msd[fit_ind], 1)
print("Slope of fit:", slope)
print("Diffusion coef:", slope/(2*3))

plt.plot(t_arr, msd)
plt.plot(t_arr[fit_ind], t_arr[fit_ind]*slope + intercept, '--')

plt.ylabel("Mean square displacement [ang]^2")
plt.xlabel("time [fs]")
plt.savefig("./msd.png", dpi=200, bbox_inches='tight')
plt.close()

