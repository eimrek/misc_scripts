#!/usr/bin/env python
import numpy as np
import ase.io

import argparse

parser = argparse.ArgumentParser(
    description='Calculates the mean square displacement from a CP2K .xyz trajectory file.')
parser.add_argument(
    'file',
    metavar='FILENAME',
    help='.xyz trajectory file containing the trajectories of molecules. \
          Assumes that data corresponding to every molecule is written on \
          sequential lines.')
parser.add_argument(
    'at_per_mol',
    type=int,
    metavar='APM',
    help="Atoms per molecule")
parser.add_argument(
    'dt',
    type=float,
    metavar='DT',
    help="Time interval between two configurations in [fs].")
parser.add_argument(
    'n_window',
    type=int,
    metavar='NWIN',
    help="Number of time steps to keep in the window. (This*dt determines cutoff time.)")
parser.add_argument(
    '--start_point_spacing',
    type=int,
    metavar='NSP',
    default=10,
    help="Spacing of timesteps to use as starting points for the calculation.")
parser.add_argument(
    '--outfile',
    metavar='FILENAME',
    default='./mean_square_disp.out',
    help="Output file.")

args = parser.parse_args()

# By default, cp2k writes velocities in [bohr/au_time] = (0.529177 ang)/(0.0242 fs)
at_pos_list = ase.io.read(args.file, index=':')

at_per_mol = args.at_per_mol
n_window = args.n_window
start_point_spacing = args.start_point_spacing

n_timesteps = len(at_pos_list)
n_mols = int(len(at_pos_list[0])/at_per_mol)

if len(at_pos_list[0]) % at_per_mol != 0:
    print("Error: number of atoms needs to divide 3 (number of atoms in the molecule).")

histogram = [[] for i in range(n_window)]
starting_point_pos = []

for i_time, at_pos in enumerate(at_pos_list):
    
    # Calculate the velocities of the mass centers of each molecule
    pos_mols = np.zeros((n_mols, 3))
    for i in range(n_mols):
        pos_atoms = at_pos.positions[at_per_mol*i:at_per_mol*(i+1)]
        at_masses = at_pos.get_masses()[at_per_mol*i:at_per_mol*(i+1)]
        for i_at in range(at_per_mol):
            pos_mols[i, :] += at_masses[i_at]*pos_atoms[i_at, :]
        pos_mols[i, :] /= np.sum(at_masses)
    
    if i_time % start_point_spacing == 0:
        starting_point_pos.append(np.copy(pos_mols))
    
    for i_sp in range(len(starting_point_pos)-1, -1, -1):
        index_delta = i_time - i_sp*start_point_spacing
        if index_delta >= n_window:
            break
        histogram[index_delta].append(np.mean(np.sum((starting_point_pos[i_sp]-pos_mols)**2, axis=1)))

        
# average over the histograms
msd = np.zeros(n_window)
for i in range(n_window):
    msd[i] = np.mean(histogram[i])

# finite difference derivative approx of msd
fin_diff = np.zeros(n_window)
for i in range(n_window):
    if i == 0:
        continue
    if i == n_window-1:
        fin_diff[i] = msd[i]-msd[i-1]
        continue
    if i == 1:
        fin_diff[0] = msd[1]-msd[0]
    fin_diff[i] = (msd[i+1]-msd[i-1])/2.0

fin_diff /= args.dt

step_nr_array = np.arange(0, n_window, 1)
t_arr = step_nr_array*args.dt
header = "step_nr  time[fs]  msd[ang^2]  derivative[ang^2/fs]"
format_str = "%4d %7.1f %12.5e %12.5e"
np.savetxt(args.outfile, np.transpose([step_nr_array, t_arr, msd, fin_diff]), fmt=format_str, header=header)
