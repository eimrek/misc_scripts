#!/usr/bin/env python
import numpy as np
import ase.io

import argparse

parser = argparse.ArgumentParser(
    description='Calculates the velocity autocorrelation from a CP2K .xyz trajectory file.')
parser.add_argument(
    'file',
    metavar='FILENAME',
    help='.xyz trajectory file containing the trajectories of molecules. \
          Assumes that data corresponding to every molecule is written on \
          sequential lines.')
parser.add_argument(
    'at_per_mol',
    type=int,
    metavar='APM',
    help="Atoms per molecule")
parser.add_argument(
    'dt',
    type=float,
    metavar='DT',
    help="Time interval between two configurations in [fs].")
parser.add_argument(
    'n_window',
    type=int,
    metavar='NWIN',
    help="Number of time steps to keep in the window. (This*dt determines cutoff time.)")
parser.add_argument(
    '--start_point_spacing',
    type=int,
    metavar='NSP',
    default=10,
    help="Spacing of timesteps to use as starting points for the calculation. Default: 10")
parser.add_argument(
    '--outfile',
    metavar='FILENAME',
    default='./vel_autocorr.out',
    help="Name of the output file.")

args = parser.parse_args()

# By default, cp2k writes velocities in [bohr/au_time] = (0.529177 ang)/(0.0242 fs)
at_vel_list = ase.io.read(args.file, index=':')

at_per_mol = args.at_per_mol
n_window = args.n_window
start_point_spacing = args.start_point_spacing

n_timesteps = len(at_vel_list)
n_mols = int(len(at_vel_list[0])/at_per_mol)

if len(at_vel_list[0]) % at_per_mol != 0:
    print("Error: number of atoms needs to divide 3 (number of atoms in the molecule).")

autocorr_histogram = [[] for i in range(n_window)]
starting_point_vels = []

for i_time, at_vel in enumerate(at_vel_list):
    
    # Calculate the velocities of the mass centers of each molecule
    vel_mols = np.zeros((n_mols, 3))
    for i in range(n_mols):
        vel_atoms = at_vel.positions[at_per_mol*i:at_per_mol*(i+1)]
        at_masses = at_vel.get_masses()[at_per_mol*i:at_per_mol*(i+1)]
        for i_at in range(at_per_mol):
            vel_mols[i, :] += at_masses[i_at]*vel_atoms[i_at, :]
        vel_mols[i, :] /= np.sum(at_masses)
        
    # Convert velocities to ang/fs
    vel_mols *= 0.529177/0.0242
    
    if i_time % start_point_spacing == 0:
        starting_point_vels.append(np.copy(vel_mols))
    
    for i_sp in range(len(starting_point_vels)-1, -1, -1):
        index_delta = i_time - i_sp*start_point_spacing
        if index_delta >= n_window:
            break
        autocorr_histogram[index_delta].append(np.mean(np.sum(starting_point_vels[i_sp]*vel_mols, axis=1)))

        
# average over the histograms
autocorr = np.zeros(n_window)
diff_coef = np.zeros(n_window)
for i in range(n_window):
    autocorr[i] = np.mean(autocorr_histogram[i])
    diff_coef[i] = diff_coef[i-1] + 1.0/3*autocorr[i]*args.dt

step_nr_array = np.arange(0, n_window, 1)
t_arr = step_nr_array*args.dt
header = "step_nr  time[fs]  vel_autocorr[ang/fs]^2  diff_coef[ang^2/fs]"
format_str = "%4d %7.1f %12.5e %12.5e"
np.savetxt(args.outfile, np.transpose([step_nr_array, t_arr, autocorr, diff_coef]), fmt=format_str, header=header)
