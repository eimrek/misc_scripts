#!/usr/bin/env python
import numpy as np              # Numerical python module, provides convenient and fast linear algebra tools
import ase.io                   # Atomistic simulation environment input-output module for reading .xyz file
import matplotlib.pyplot as plt # Matplotlib for plotting
import argparse                 # For parsing command line arguments

# Define possible command-line arguments and help messages for each
parser = argparse.ArgumentParser(
    description='Calculates the simple mean square displacement from a CP2K .xyz trajectory file.')
parser.add_argument(
    'file',
    metavar='FILENAME',
    help='.xyz trajectory file containing the trajectories of molecules. \
          Assumes that data corresponding to every molecule is written on \
          sequential lines.')
parser.add_argument(
    'at_per_mol',
    type=int,
    metavar='APM',
    help="Atoms per molecule")
parser.add_argument(
    'dt',
    type=float,
    metavar='DT',
    help="Time interval between two configurations in [fs].")

# parse the arguments: each argument is now accessible by args.<name>
args = parser.parse_args()
at_per_mol = args.at_per_mol # rename to make a little bit shorter to use later

# Read the .xyz trajectory file, it will contain a _Python list_ of ase.Atoms objects
# Each element of this list contain a configuration of all atoms,
# which corresponds to a time moment
at_pos_list = ase.io.read(args.file, index=':')

n_timesteps = len(at_pos_list) # total number of time values
n_mols = int(len(at_pos_list[0])/at_per_mol) # number of molecules

if len(at_pos_list[0]) % at_per_mol != 0:
    print("Error: number of atoms needs to divide 3 (number of atoms in the molecule).")
    exit()

# Define a numpy array which will hold the msd for each molecule
# (row = molecule index, col = timestep index)
msd_for_each_mol = np.zeros((n_mols, n_timesteps))

# loop over all timesteps
# (i_time is the index and at_pos contains the corresponding ase.Atoms object)
for i_time, at_pos in enumerate(at_pos_list):
    
    # ---------
    # Calculate the positions of the mass centers of each molecule
    pos_mols = np.zeros((n_mols, 3))
    for i in range(n_mols):
        # Numpy array slicing: [a:b] returns another numpy array with only rows "a" to "b-1"
        # ase.Atoms object has many useful members, e.g. .positions and .get_masses() (both return numpy arrays)
        pos_atoms = at_pos.positions[at_per_mol*i:at_per_mol*(i+1)]
        at_masses = at_pos.get_masses()[at_per_mol*i:at_per_mol*(i+1)]
        for i_at in range(at_per_mol):
            # pos_atoms[i] contains a numpy array of three coordinates of atom i
            # addition works elementwise: [mol_x, mol_y, mol_z] += atomic_mass * [at_x, at_y, at_z]
            pos_mols[i] += at_masses[i_at]*pos_atoms[i_at]
        # Divide by the mass of the whole molecule to get the position of the center of mass
        pos_mols[i] /= np.sum(at_masses)
    # ---------

    if i_time == 0:
        # On the first time moment, copy the configuration of atoms to a new variable
        # Usually in Python, in the case of simple assignment "a=b",
        # "a" will be an alias of b, which means, that when changing "b",
        # also "a" changes. To prevent this, we use np.copy()
        initial_positions = np.copy(pos_mols)
        # Additionally, note that in python, variables are not defined in a local scope
        # note that we can use initial_positions outside the scope of the current conditional

    for i in range(n_mols):
        delta_r = pos_mols[i] - initial_positions[i]
        # np.sum(delta_r**2) first does elementwise squaring and then sums all elements of the array
        # so, the length of delta_r; could have also used np.dot(delta_r, delta_r)
        msd_for_each_mol[i, i_time] = np.sum(delta_r**2)

# average msd over the molecules
avg_msd = np.mean(msd_for_each_mol, axis=0)

# plotting
t_arr = np.arange(0, n_timesteps, 1)*args.dt
for i in range(n_mols):
    plt.plot(t_arr, msd_for_each_mol[i, :])
plt.plot(t_arr, avg_msd, 'k-', lw=3.0)
plt.ylabel("Mean square displacement [ang]^2")
plt.xlabel("time [fs]")
plt.savefig("./msd_simple.png", dpi=200, bbox_inches='tight')
plt.show()
