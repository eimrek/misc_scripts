#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser(
    description='Plots the velocity autocorrelation and diffusion coefficient.')
parser.add_argument(
    'file',
    metavar='FILENAME',
    help='Output of vel_autocorr calculation')

args = parser.parse_args()

data = np.loadtxt(args.file)

plt.plot(data[:, 1], data[:, 2])
plt.ylabel("velocity autocorrelation [ang/fs]^2")
plt.xlabel("time [fs]")
plt.savefig("./vel_autocorr.png", dpi=200, bbox_inches='tight')
plt.close()

plt.plot(data[:, 1], data[:, 3])
plt.ylabel("Diffusion coeff [ang^2/fs]")
plt.xlabel("time [fs]")
plt.savefig("./diff_coef.png", dpi=200, bbox_inches='tight')
plt.close()
