#!/usr/bin/env python3
import numpy as np
import sys
import os

from PIL import Image, ImageDraw, ImageFont, ImageOps

if len(sys.argv) <= 4:
    print('Please specify [file name] [label] [x_pos] [y_pos].')
    sys.exit(1)

filename = sys.argv[1]

label = sys.argv[2]

x_pos = float(sys.argv[3])
y_pos = float(sys.argv[4])


image = Image.open(filename)

width, height = image.size

# expand border
border = 300
new_width = width + border * 2
new_height = height + border * 2

expanded_image = Image.new("RGBA", (new_width, new_height), (0,0,0,0))
expanded_image.paste(image, (border, border))

pos_px = (border + x_pos * width, border + y_pos * height)

script_location = os.path.dirname(os.path.realpath(__file__))

font = ImageFont.truetype(script_location+"/font/inter-regular.ttf", 120)

draw = ImageDraw.Draw(expanded_image)

w, h = draw.textsize(label, font=font)
draw.text((pos_px[0] - w/2.0, pos_px[1] - h/2.0), label, (0,0,0), font=font)

expanded_image = expanded_image.crop(expanded_image.getbbox())


name, ext = os.path.splitext(filename)
expanded_image.save(name+"_label"+ext)
